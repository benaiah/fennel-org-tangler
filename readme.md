License: MIT

Using Fennel, licensed under MIT, from https://github.com/bakpakin/fennel

Using lua-resty-tsort, licensed under BSD, from https://github.com/bungle/lua-resty-tsort
