(local {: view} (require :lib.fennel))

(fn split-string-by-whitespace [str]
  (let [results []]
    (var results-len 0)
    (each [item (str:gmatch "%S+")]
      (set results-len (+ results-len 1))
      (tset results results-len item))
    results))

(local header-args-key-pattern ":%w")

(fn header-args-from-tokens [tokens]
  ;; the _keys property on header args objects is used to preserve the
  ;; original order of the header args
  (let [results {:_keys []}]
    (var last-key nil)
    (each [_ token (ipairs tokens)]
      (if (token:match header-args-key-pattern)
          (do (when (and last-key (= nil (. results last-key)))
                (error (.. "no values were provided for header arg key " last-key)))
              (set last-key (token:sub 2))
              (table.insert results._keys last-key))
          (let [current-results (. results last-key)
                new-results (if (and current-results (= :table (type current-results)))
                                (do (table.insert current-results token) current-results)
                                current-results
                                [current-results token]
                                (= current-results nil)
                                token)]
            (tset results last-key new-results))))
    results))

(fn parse-src-block-header [src-block-header-string]
  (let [tokens (split-string-by-whitespace src-block-header-string)
        [language & header-args-tokens] tokens
        header-args (header-args-from-tokens header-args-tokens)]
    {: language : header-args}))

{: parse-src-block-header}
