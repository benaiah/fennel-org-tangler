(local {: view} (require :lib.fennel))
(local {: new-pushlist : push} (require :util))
(local {: file->chunks : file->lines : chunks->lines} (require :streams))
(local {: parse-src-block-header} (require :parse-src-block-headers))
(local {: new-src-block : format-src-block} (require :src-blocks))
(local {: build-dependency-graph : resolve-links} (require :noweb))
(local {: create-parents-and-open-file-for-writing} (require :filesystem))

(var show-help nil)
(var filename nil)
(var output-prefix "./")
(fn set-output-prefix [op]
  (set output-prefix
       (if (= (. op (length op)) :/) op
           (.. op :/))))

(var arg-pos 1)
(var accept-flags? true)
(while (not= arg-pos -1)
  (if accept-flags? 
      (match (values (. arg arg-pos) (. arg (+ arg-pos 1)))
        nil (set arg-pos -1)
        :--help (do (set show-help true) (set arg-pos (+ arg-pos 1)))
        :-h (do (set show-help true) (set arg-pos (+ arg-pos 1)))
        (:--output op) (do (set-output-prefix op) (set arg-pos (+ arg-pos 2)))
        (:-o op) (do (set-output-prefix op) (set arg-pos (+ arg-pos 2)))
        (:--) (do (set accept-flags? false) (set arg-pos (+ arg-pos 1)))
        str (do (set filename str) (set arg-pos (+ arg-pos 1)))
        _ (error "unrecognized arguments"))
      (if (and filename (. arg arg-pos)) (error "filename cannot be set twice")
          (. arg arg-pos) (do (set filename (. arg arg-pos)) (set arg-pos (+ arg-pos 1)))
          (set arg-pos -1))))

(when show-help
  (print "

Usage: fennel-org-tangler [FLAG] [FILE]

Tangle all source blocks from a file into their assigned destination files.

  --help (-h)	Display this help message
  --output (-o)	Output files to the specified folder
")
  (os.exit))

(local input-file (if (or (= filename nil) (= filename "-")) io.stdin
                      (io.open filename :r)))

(local header-pattern "^(%*+) (.+)\n$")
(local header-title-comment-pattern "^COMMENT .+$")
(local header-title-todo-comment-pattern "^[TD]O[DN][EO] COMMENT .+$")
(local src-begin-line-pattern "^#%+[Bb][Ee][Gg][Ii][Nn]_[Ss][Rr][Cc](.*)\n$")
(local src-end-line-pattern "^#%+[Ee][Nn][Dd]_[Ss][Rr][Cc](.*)\n$")

(fn tangle []
  (let [src-blocks-by-name {}
        src-blocks-order (new-pushlist)
        content-lines-by-name {}
        headers-by-name {}
        set-headers-for-name #(tset headers-by-name $1 $2)
        generate-name-for-nameless-block
        (do (var i 0)
            (fn []
              (set i (+ i 1))
              (.. "anonymous-" i)))
        chunk-stream (file->chunks input-file)
        line-stream (chunks->lines chunk-stream)
        resolved-contents-by-name {}]
    (var collecting-lines-into nil)
    (var commented-heading-level 0)
    (each [line line-stream]
      (let [src-block-args-str (line:match src-begin-line-pattern)
            (header-asterisks header-title)
            (when (not src-block-args-str) (line:match header-pattern))]
        (if (and src-block-args-str (= commented-heading-level 0))
            (let [{: language : header-args} (parse-src-block-header src-block-args-str)
                  {: noweb-ref} header-args
                  name (or noweb-ref (generate-name-for-nameless-block))]
              (push src-blocks-order name)
              (set-headers-for-name name {: language : header-args})
              (tset content-lines-by-name name (new-pushlist))
              (set collecting-lines-into name))

            (line:match src-end-line-pattern)
            (set collecting-lines-into nil)

            (and header-asterisks (or (header-title:match header-title-comment-pattern)
                                      (header-title:match header-title-todo-comment-pattern)))
            (set commented-heading-level
                 (if (= commented-heading-level 0) (length header-asterisks)
                     (> commented-heading-level (length header-asterisks)) (length header-asterisks)
                     commented-heading-level))

            header-asterisks
            (when (>= commented-heading-level (length header-asterisks))
              (set commented-heading-level 0))

            collecting-lines-into
            (push (. content-lines-by-name collecting-lines-into) line))))

    ;; create src block objects and add the contents of all blocks
    ;; without links to resolved-contents-by-name
    (each [_ name (ipairs src-blocks-order)]
      (let [{: language : header-args} (. headers-by-name name)
            contents (table.concat (. content-lines-by-name name) "")
            contents-without-trailing-newline (string.sub contents 1 (- (length contents) 1))
            src-block (new-src-block {: name : language : header-args
                                      :contents contents-without-trailing-newline})]
        (tset src-blocks-by-name name src-block)
        (when (= 0 (length src-block.links))
          (tset resolved-contents-by-name src-block.name contents-without-trailing-newline))))

    ;; resolve the final contents of all the source blocks
    (let [graph (build-dependency-graph src-blocks-by-name)
          (sorted err) (graph:sort)]
      (when err (error err))
      (let [sorted-len (length sorted)]
        (for [i 0 (- sorted-len 1)]
          (let [index (- sorted-len i)
                name (. sorted index)
                src-block (. src-blocks-by-name name)
                _ (when (not src-block) (error (.. "reference found to a nonexistent src block: " name)))
                resolved-contents (or (. resolved-contents-by-name name)
                                      (resolve-links src-block resolved-contents-by-name))]
            (tset resolved-contents-by-name src-block.name resolved-contents)))))

    ;; perform the actual tangling to final files
    (each [_ block (pairs src-blocks-by-name)]
      (let [resolved-content (. resolved-contents-by-name block.name)]
        (when block.header-args.tangle
          (let [f-name (.. output-prefix block.header-args.tangle)
                f (create-parents-and-open-file-for-writing f-name)]
            (print "WRITING FILE " f-name)
            (f:write resolved-content)
            (f:write "\n")
            (f:close)))))))

(tangle)
