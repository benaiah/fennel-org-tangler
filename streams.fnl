(local {: view} (require :lib.fennel))
(local {: new-pushlist : push} (require :util))

(local default-chunk-size (^ 2 12)) ;; 4kb
(fn file->chunks [file chunk-size]
  (let [cs (or chunk-size default-chunk-size)]
    #(let [chunk (file:read cs)]
       (when (not chunk) (file:close))
       chunk)))

(fn file->lines [file]
  #(let [line (file:read :*l)]
     line))

(fn empty-buffer [buffer index]
  (let [i (or index 1)]
    (tset buffer i nil)
    (when (<= i (length buffer))
      (empty-buffer buffer (+ i 1)))
    (set buffer.n 0)
    buffer))

(fn chunks->lines [chunk-stream]
  (let [incomplete-line-buffer (new-pushlist)]
    (var chunk-substring-index 1)
    (var chunk (chunk-stream))
    (fn loop []
      (if
       ;; if we're at the end of the input stream and there's no
       ;; partial line to output, end the stream
       (and (= chunk nil) (= (length incomplete-line-buffer) 0))
       (values)

       ;; if we're at the end of the input stream and there's an
       ;; unfinished line no newline at the end, return the final line
       (= chunk nil)
       (let [ret (table.concat incomplete-line-buffer "")]
         (empty-buffer incomplete-line-buffer)
         ret)

       ;; if we have an incomplete line in the buffer, see if we can finish it
       (> (length incomplete-line-buffer) 0)
       (let [next-newline-index (string.find chunk "\n")]
         (if
          ;; if we can't finish the line, push the chunk onto the
          ;; incomplete line buffer and grab a new chunk
          (= nil next-newline-index)
          (do (push incomplete-line-buffer chunk)
              (set chunk-substring-index 1)
              (set chunk (chunk-stream))
              (loop))

          ;; if we can finish the line, do so
          (do (push incomplete-line-buffer (string.sub chunk 1 next-newline-index))
              (set chunk-substring-index (+ next-newline-index 1))
              (let [ret (table.concat incomplete-line-buffer "")]
                (empty-buffer incomplete-line-buffer)
                ret))))

       ;; if we've already moved past the end of the current chunk, get a new one and loop
       (> chunk-substring-index (length chunk))
       (do (set chunk-substring-index 1)
           (set chunk (chunk-stream))
           (loop))

       ;; if we're partway thru returning the lines of a string in the
       ;; lines buffer, return the next one
       (and (> chunk-substring-index 1))
       (let [next-newline-index (string.find chunk "\n" chunk-substring-index true)]
         (if (= next-newline-index nil)
             (do (push incomplete-line-buffer (string.sub chunk chunk-substring-index (length chunk)))
                 (set chunk-substring-index 1)
                 (set chunk (chunk-stream))
                 (loop))

             (let [ret (string.sub chunk chunk-substring-index next-newline-index)]
               (set chunk-substring-index (+ next-newline-index 1))
               ret)))

       ;; we are just starting to look at a new chunk
       (let [next-newline-index (string.find chunk "\n" 1 true)]
         (if (= next-newline-index nil)
             (do (push incomplete-line-buffer chunk)
                 (set chunk (chunk-stream))
                 (loop))

             (let [ret (string.sub chunk 1 next-newline-index)]
               (set chunk-substring-index (+ next-newline-index 1))
               ret)))))))

{: file->chunks
 : file->lines
 : chunks->lines}
