(local link-mt {
                ;; :__fennelview
                ;; (fn [link metamethod options indent]
                ;;   (let [ret ["{"]]
                ;;     (each [_ key (ipairs [:name :start :end])]
                ;;       (table.insert ret (.. :: key " " (. link key))))
                ;;     (table.insert ret "}")
                ;;     ret))
                })

(fn new-link [name start end]
  (let [link {: name : start : end}]
    (setmetatable link link-mt)))

{: new-link}
