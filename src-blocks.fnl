(local {: view} (require :lib.fennel))
(local {: new-link} (require :links))

(local link-pattern "<<[%w%-%?]+>>")

(fn find-links [contents]
  (let [results []]
    (var (start end) (string.find contents link-pattern))
    (while (and start end)
      (let [name (string.sub contents (+ start 2) (- end 2))
            link (new-link name start end)]
        (table.insert results link))
      (set (start end) (string.find contents link-pattern end)))
    results))

(local src-block-mt
       {:__fennelview
        (fn [{: name : language : header-args : links} metamethod options indent]
          (metamethod {: name : language : header-args : links :contents "..."}
                      options indent))
        })

(fn new-src-block [{: name : language : header-args : contents}]
  ;; (print (view contents))
  (let [links (find-links contents)
        ret {: name
             : language
             : header-args
             : contents
             : links}]
    (setmetatable ret src-block-mt)))

(fn format-header-args [header-args]
  (if header-args
      (let [results []]
        (each [_ key (ipairs header-args._keys)]
          (table.insert results (.. ":" key " " (view (. header-args key)))))
        (table.concat results " "))
      ""))

(fn format-src-block [{: name : language : header-args : contents}]
  (let [formatted-header-args (format-header-args header-args)]
    (.. name " " (or language "") " " formatted-header-args "\n"
        "--------------------------------------------------\n"
        contents
        "--------------------------------------------------\n\n")))

{: new-src-block : format-src-block}
