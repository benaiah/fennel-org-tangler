(local lfs (require :lfs))
(local {: new-pushlist : push} (require :util))

(fn create-parents [path]
  (local dirs (new-pushlist))
  (var dirpath ".")
  (each [dir (path:gmatch "([^/]+)")]
    (push dirs dir))
  (each [i dir (ipairs dirs)]
    (set dirpath (.. dirpath :/ dir))
    (when (< i dirs.n)
      (let [(attrs err) (lfs.attributes dirpath)]
        (when (and attrs (not= attrs.mode :directory))
          (error (.. "path " dirpath " already exists but is not a directory")))
        (when (and err (err:match "No such file or directory$"))
          (let [(succeeded? err) (lfs.mkdir dirpath)]
            (when (not succeeded?)
              (error err))))))))

(fn create-parents-and-open-file-for-writing [path]
  (let [(f err) (io.open path :w)]
    (match (values f err)
      (f ? (not= f nil)) f
      ((nil err) ? (err:match "No such file or directory$"))
      (do (create-parents path)
          (let [(f err) (io.open path :w)]
            (when err
              (error (.. "failed to create file: " err)))
            f))
      _ (error err))))

{: create-parents-and-open-file-for-writing}
