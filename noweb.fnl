(local tsort (require :lib.tsort))
(local {: new-pushlist : push} (require :util))

(fn build-dependency-graph [src-blocks]
  (let [graph (tsort.new)]
    (each [_ src-block (pairs src-blocks)]
      (let [{: name : links} src-block]
        (each [_ link (ipairs links)]
          (graph:add name link.name))))
    graph))

(fn resolve-links [src-block resolved-contents-by-name]
  (let [{: name : links : contents} src-block
        resolved-content-sections-for-this-src-block (new-pushlist)]
    (var previous-end 0)
    (each [i {:name linked-to : start : end} (ipairs links)]
      (let [next-link (. links (+ i 1))
            before-link (string.sub contents (+ previous-end 1) (- start 1))
            after-link
            (if next-link
                (let [{:start next-link-start} next-link]
                  (string.sub contents (+ end 1) (- next-link-start 1)))
                (string.sub contents (+ end 1) (length contents)))
            resolved-link-contents (. resolved-contents-by-name linked-to)]
        (when (not resolved-link-contents)
          (error (.. "the contents of " linked-to " have not yet been resolved")))
        (push resolved-content-sections-for-this-src-block before-link)
        (push resolved-content-sections-for-this-src-block resolved-link-contents)
        (push resolved-content-sections-for-this-src-block after-link)
        (set previous-end end)))
    (table.concat resolved-content-sections-for-this-src-block "")))

{: build-dependency-graph : resolve-links}
