(local {: view} (require :lib.fennel))

;; using the n property matches the structure of the return valuse of
;; table.pack
(local pushlist-mt {:__len (fn [{: n}] n)
                    :__fennelview
                    (fn [[& vals]] (view vals))})

(fn new-pushlist []
  (let [list {:n 0}]
    (setmetatable list pushlist-mt)
    list))

(fn push [pushlist item]
  (set pushlist.n (+ pushlist.n 1))
  (tset pushlist pushlist.n item))

{: new-pushlist
 : push}
